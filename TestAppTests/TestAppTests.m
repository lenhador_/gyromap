//
//  TestAppTests.m
//  TestAppTests
//
//  Created by Aleksandr Belov on 06.07.14.
//  Copyright (c) 2014 Aleksandr Belov. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface TestAppTests : XCTestCase

@end

@implementation TestAppTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
