//
//  WebViewController.m
//  TestApp
//
//  Created by Aleksandr Belov on 07.07.14.
//  Copyright (c) 2014 Aleksandr Belov. All rights reserved.
//

#import "WebViewController.h"
#define YANDEX_URL    @"http://yandex.ru"

@interface WebViewController ()
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@end

@implementation WebViewController

#pragma mark - UIViewController methods

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.title = @"Yandex";
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self loadRequestToYandex];
}


#pragma mark - Private methods

- (void)loadRequestToYandex {
    
    NSURL *url = [NSURL URLWithString:YANDEX_URL];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [_webView loadRequest:request];
}

@end
