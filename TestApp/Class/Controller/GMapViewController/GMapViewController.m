//
//  GMapViewController.m
//  TestApp
//
//  Created by Aleksandr Belov on 06.07.14.
//  Copyright (c) 2014 Aleksandr Belov. All rights reserved.
//

#import "GMapViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import <CoreMotion/CoreMotion.h>
#import "Utils.h"
#import "MainSnippet.h"

#define MAX_ANGEL               1.570796
#define GYRO_UPDATE_INTERVAL    1.
@interface GMapViewController () <
    GMSMapViewDelegate,
    UIAlertViewDelegate,
    CLLocationManagerDelegate
> {
    BOOL _firstLocationUpdate;
    CGFloat _currentAngle;
}
@property (strong, nonatomic) IBOutlet GMSMapView *mapView;
@property (strong, nonatomic) GMSGeocoder *geocoder;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) CMMotionManager *motionManager;
@end

@implementation GMapViewController


#pragma mark - UIViewController methods

- (void)viewDidLoad {

    [super viewDidLoad];
    self.title = @"Map";
    if (!_locationManager) {
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
    }
    [_locationManager startUpdatingLocation];
    _mapView.delegate = self;
}


#pragma mark - GMSMapViewDelegate

- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate {
    
    [_mapView clear];
    [self addMarkerWithLocation:coordinate];
}

- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker {
    
    [[[UIAlertView alloc] initWithTitle:nil
                                message:[NSString stringWithFormat:@"You chose %@", marker.title]
                               delegate:self
                      cancelButtonTitle:@"done"
                      otherButtonTitles:@"remove", nil] show];

}

#pragma mark - UIAlertView delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 1) {
        [_mapView clear];
    }
}

#pragma mark - Private methods

- (CGFloat)motionAtRadians {
    
    return self.view.frame.size.width / MAX_ANGEL;
}

- (CGFloat)calculateMotionWithRadians:(CGFloat)radians {
    
    return (self.motionAtRadians * radians);
}

- (void)proccessGyroData:(CMGyroData *)data {
   
    [[MainSnippet snippet] hideSnippet];
    CMRotationRate rotation = data.rotationRate;
    if (!(_currentAngle >= MAX_ANGEL) && !(_currentAngle <= -(MAX_ANGEL))) {
        _currentAngle += rotation.y;
    }
    CGFloat xMotion = [self calculateMotionWithRadians:_currentAngle];
    [_mapView moveCamera:[GMSCameraUpdate scrollByX:xMotion Y:0]];
}

- (void)changeTiltModeTo:(BOOL)on {
    
    if (on) {
        [[MainSnippet snippet] showSnippetWithParent:self];
        _mapView.settings.scrollGestures = NO;
    } else {
        if ([MainSnippet snippet].isActive) {
            [[MainSnippet snippet] hideSnippet];
        }
        _currentAngle = 0;
        _mapView.settings.scrollGestures = YES;
    }
    UIColor *color = on ? BLUE_APP_COLOR : GRAY_APP_COLOR;
    [self.navigationItem.leftBarButtonItem setTitleTextAttributes:@{NSForegroundColorAttributeName:color, NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Thin" size:16.]}
                                                         forState:UIControlStateNormal];
}

- (void)addMarkerWithLocation:(CLLocationCoordinate2D)location {
    
    GMSReverseGeocodeCallback handler = ^(GMSReverseGeocodeResponse *response, NSError *error) {
        GMSAddress *address = response.firstResult;
        if (address) {
            GMSMarker *marker = [GMSMarker markerWithPosition:location];
            marker.title = [[address lines] firstObject];
            marker.snippet = [Utils locationCoordinateToString:location];
            marker.appearAnimation = kGMSMarkerAnimationPop;
            marker.map = _mapView;
        } else {
            NSLog(@"Could not reverse geocode point (%f,%f): %@",
                  location.latitude, location.longitude, error);
        }
    };
    
    if (!_geocoder) {
        _geocoder = [[GMSGeocoder alloc] init];
    }
    [_geocoder reverseGeocodeCoordinate:location completionHandler:handler];
}


#pragma mark - Actions

- (IBAction)buttonTiltModeTapped:(UIBarButtonItem *)sender {

    if (!_motionManager.isGyroActive) {
        if (!_motionManager) {
            _motionManager = [[CMMotionManager alloc] init];
            _motionManager.gyroUpdateInterval = GYRO_UPDATE_INTERVAL;
        }
        [self changeTiltModeTo:YES];
        [_motionManager startGyroUpdatesToQueue:[NSOperationQueue currentQueue] withHandler:^(CMGyroData *gyroData, NSError *error) {
            if (error) {
                NSLog(@"We got error: %@", error);
            } else {
                [self proccessGyroData:gyroData];
            }
        }];
    } else {
        [_motionManager stopGyroUpdates];
        [self changeTiltModeTo:NO];
    }
}


#pragma mark - CLLocationManger delegate methods


- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    if (!_firstLocationUpdate) {
        _firstLocationUpdate = YES;
        CLLocation *location = [locations lastObject];
        if (location) {
            _mapView.camera = [GMSCameraPosition cameraWithTarget:location.coordinate
                                                             zoom:14];
            [_locationManager stopUpdatingLocation];
            _locationManager = nil;
        }
    }
}

@end
