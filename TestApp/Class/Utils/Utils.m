//
//  Utils.m
//  TestApp
//
//  Created by Aleksandr Belov on 07.07.14.
//  Copyright (c) 2014 Aleksandr Belov. All rights reserved.
//

#import "Utils.h"

@implementation Utils

+ (NSString *)locationCoordinateToString:(CLLocationCoordinate2D)location {
    
    if (CLLocationCoordinate2DIsValid(location)) {
        return [NSString stringWithFormat:@"%f, %f", location.latitude, location.longitude];
    }    
    return nil;
}

+ (CGFloat)radiansToDegrees:(CGFloat)radians {
    
    return radians * (180/M_PI);
}

+ (CGFloat)degreesToRadians:(CGFloat)degrees {
    
    return degrees / 180 * M_PI;
}
@end
