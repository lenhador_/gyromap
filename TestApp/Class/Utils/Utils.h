//
//  Utils.h
//  TestApp
//
//  Created by Aleksandr Belov on 07.07.14.
//  Copyright (c) 2014 Aleksandr Belov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface Utils : NSObject

+ (NSString *)locationCoordinateToString:(CLLocationCoordinate2D)location;
+ (CGFloat)radiansToDegrees:(CGFloat)radians;
+ (CGFloat)degreesToRadians:(CGFloat)degrees;
@end
