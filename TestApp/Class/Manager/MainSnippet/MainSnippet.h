//
//  MainSnippet.h
//  TestApp
//
//  Created by Aleksandr Belov on 07.07.14.
//  Copyright (c) 2014 Aleksandr Belov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MainSnippet : NSObject

@property (nonatomic, readonly) BOOL isActive;

+ (instancetype)snippet;
- (void)showSnippetWithParent:(UIViewController *)parent;
- (void)hideSnippet;
@end
