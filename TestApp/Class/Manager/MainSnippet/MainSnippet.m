//
//  MainSnippet.m
//  TestApp
//
//  Created by Aleksandr Belov on 07.07.14.
//  Copyright (c) 2014 Aleksandr Belov. All rights reserved.
//

#import "MainSnippet.h"
#define DURATION  0.3

@interface MainSnippet ()
@property (strong, nonatomic) UIActivityIndicatorView *snippet;
@property (strong, nonatomic) UIView *contentView;
@end

@implementation MainSnippet


#pragma mark - Init methods

+ (instancetype)snippet {
    
    static MainSnippet *snippet = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        snippet = [[MainSnippet alloc] init];
    });
    return snippet;
}


#pragma mark - Public methods

- (void)showSnippetWithParent:(UIViewController *)parent {
    
    if (!_isActive) {
        _isActive = YES;
        if (!_contentView) {
            _contentView = [[UIView alloc] init];
            _contentView.backgroundColor = [UIColor whiteColor];
            _snippet = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            [_contentView addSubview:_snippet];
            _contentView.alpha = 0.;
        }
        _contentView.frame = parent.view.frame;
        CGFloat xx = CGRectGetMidX(parent.view.frame);
        CGFloat yy = CGRectGetHeight([UIScreen mainScreen].bounds) * 0.1;
        if (parent.navigationController) {
            yy += parent.navigationController.navigationBar.frame.size.height;
        }
        if (parent.tabBarController) {
            yy += parent.tabBarController.tabBar.frame.size.height;
        }
        _snippet.center = CGPointMake(ceilf(xx), ceilf(yy));
        
        [UIView animateWithDuration:DURATION animations:^{
            [parent.view addSubview:_contentView];
            _contentView.alpha = 1.;
        } completion:^(BOOL finished) {
            [_snippet startAnimating];
        }];
    }
}

- (void)hideSnippet {
    
    if (!_isActive) {
        return;
    }
    _isActive = NO;
    [UIView animateWithDuration:DURATION animations:^{
        _contentView.alpha = 0.;
    } completion:^(BOOL finished) {
        [_contentView removeFromSuperview];
        [_snippet stopAnimating];
    }];
}

@end
