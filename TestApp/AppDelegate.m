//
//  AppDelegate.m
//  TestApp
//
//  Created by Aleksandr Belov on 06.07.14.
//  Copyright (c) 2014 Aleksandr Belov. All rights reserved.
//

#import "AppDelegate.h"
#import <GoogleMaps/GoogleMaps.h>

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [GMSServices provideAPIKey:GMS_SERVICE_API_KEY];
    [self applyTheme];
    return YES;
}

- (void)applyTheme {
    
    [[UINavigationBar appearance] setTintColor:GRAY_APP_COLOR];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:GRAY_APP_COLOR,
                                                           NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Thin" size:20.]}];
    [[UIBarButtonItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:GRAY_APP_COLOR,
                                                           NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Thin" size:16.]}
                                                forState:UIControlStateNormal];
}

@end
